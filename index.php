<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/ed-grid-css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  
	<title>Pokemon</title>
</head>
<header>
	<div class="col-md-6 .img-responsive center-block">
   		<img id="imagen" src="img/logo.png" class="img-responsive center-block" alt="titulo">
   		<div class="h-30"></div>
	</div>
</header>
<body>	
<div class="container">
  
  	<button id="electric" type="button" class="btn btn-primary">
	 Get Electric pokemons
	</button>
	<button id="raichu" type="button" class="btn btn-primary">
	 	Get Raichu Info
	</button>
	<div id="num">
		Amount of pokemons
		<span id="numeros"></span>
	</div>
	<div id="info">
		Raichu in
	</div>
  <div id="electricos_content">
    <table class="table">
		<thead>
		    <tr>
		      <th scope="col">Nombre</th>
		      <th scope="col">URL</th>
		    </tr>
		  </thead>
		  <tbody>
		    
	     	<td><span id="nombre"></span></td>
			<td><span id="poke_url"></span></td>
		    
		  </tbody>
	</table>
  </div>
  <div id="raichu_content">
  	<label>Name: </label>
  	<span id="nombre_r"></span>
  	<br>
  	<label>Type: </label>
  	<span id="tipo_r"></span>
	<br>
  	<h1>Abilities</h1><br>
  	<span id="habilidades_r"></span>
  </div>
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>