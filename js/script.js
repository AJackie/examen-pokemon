$(function(){
	console.log('ready')
	$("#raichu_content").hide()
	$("#num").hide()
	$("#info").hide()
	$("#electricos_content").hide()
	var electric = $("#electric")
		url = "https://pokeapi.co/api/v2/type/13"
		nombre = $("#nombre")
		poke_url = $("#poke_url")
		numeros = $("#numeros")
		raichu = $("#raichu")
		url_r = "https://pokeapi.co/api/v2/pokemon/raichu/"
		nombre_r = $("#nombre_r")
		tipo_r= $("#tipo_r")
		habilidades_r = $("#habilidades_r")

	electric.click(function(){
		$("#electricos_content").show()
		$("#num").show()
		$("#raichu_content").hide()
		$("#info").hide()
		$.ajax({
			type: "GET",
			url : url,
			success: function(pokemones){

				$.each(pokemones.pokemon, function(i,val){
					numeros.html(i, "=>", val.pokemon["name"])
					nombre.append('<li>' + val.pokemon["name"] + '</li>')
					poke_url.append('<li>' + val.pokemon["url"] + '</li>')
				})
			}
		})
	})

	raichu.click(function(){
		$("#electricos_content").hide()
		$("#num").hide()
		$("#raichu_content").show()
		$("#info").show()
		$.ajax({
			type: "GET",
			url : url_r,
			success: function(raichu){
				console.log(raichu.name)
				nombre_r.append(raichu.name)

				$.each(raichu.types, function(i, val){
					console.log(val.type["name"])
					tipo_r.append(val.type["name"])
				})

				$.each(raichu.abilities, function(i, val){
					console.log(val.ability["name"])
					habilidades_r.append('<li>' + val.ability["name"] + '</li>')
				})


			}
		})
	})
})